<!-- PROJECT SHIELDS -->

[![Build Status][build-shield]]()
[![Contributors][contributors-shield]]()

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/irfanrona/inventory-php-only">
  </a>

  <h3 align="center">Inventory App</h3>

  <p align="center">
    Simple Inventory App Raw PHP.
    <br />
  </p>
</p>

<!-- ABOUT THE PROJECT -->

## About The Project

This is an Simple Inventory App. Currently developed on web app.

<!-- GETTING STARTED -->

## Getting Started

This Project saved in order to fix problem from previously project !, and no longer update!

### Prerequisites

Things you need to use the software and how to install them.

- xampp (php and Mysql)

```sh
https://www.apachefriends.org/index.html
```

### Installation

1. Clone the repo to your htdocs

```sh
git clone https://gitlab.com/irfanrona/inventory-php-only.git
```

2. Create db on Mysql and import the database

```sh
inventory.sql
```

3. Configure the `config.php` and `database.php`

```sh
'database' => 'your_db_name'
```

<!-- FEATURE EXAMPLES -->

## Last Update

Here is the lastest fixing update about this app.

### General

1. New login design

### Admin

1. Create useful info in Dashboard

### Employee

1. Employee no longer have access to dashboard
2. Employee no longer have access to report feature
3. Employee cant edit and delete inventory

<!-- CONTACT -->

## Contact

Irfan Rona - [irfanrona](https://linkedin.com/in/irfanrona) - irfanrona@student.upi.edu

Project Link: [https://gitlab.com/irfanrona/inventory-php-only](https://gitlab.com/irfanrona/inventory-php-only)

<!-- ACKNOWLEDGEMENTS -->

## Acknowledgements

- [GitHub Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet)
- [Img Shields](https://shields.io)
- [GitHub Pages](https://pages.github.com)

<!-- MARKDOWN LINKS & IMAGES -->

[build-shield]: https://img.shields.io/badge/build-passing-brightgreen.svg?style=flat-square
[contributors-shield]: https://img.shields.io/badge/contributors-1-orange.svg?style=flat-square
[product-screenshot]: https://raw.githubusercontent.com/othneildrew/Best-README-Template/master/screenshot.png
