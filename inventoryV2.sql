/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100413
 Source Host           : localhost:3306
 Source Schema         : inventory

 Target Server Type    : MySQL
 Target Server Version : 100413
 File Encoding         : 65001

 Date: 28/08/2021 22:33:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tblbarang
-- ----------------------------
DROP TABLE IF EXISTS `tblbarang`;
CREATE TABLE `tblbarang`  (
  `kodebarang` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `namabarang` varchar(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `stok` int(11) NOT NULL,
  PRIMARY KEY (`kodebarang`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tblbarang
-- ----------------------------
INSERT INTO `tblbarang` VALUES ('BRG-0251', 'Teh Gelas', 0);
INSERT INTO `tblbarang` VALUES ('BRG-0871', 'Sunsilk', 6);
INSERT INTO `tblbarang` VALUES ('BRG-3452', 'Taro', 20);
INSERT INTO `tblbarang` VALUES ('BRG-5619', 'Sarimi', 20);
INSERT INTO `tblbarang` VALUES ('BRG-6372', 'Permen', 15);
INSERT INTO `tblbarang` VALUES ('BRG-7025', 'Teh Pucuk', 25);
INSERT INTO `tblbarang` VALUES ('BRG-9043', 'Potatos', 100);
INSERT INTO `tblbarang` VALUES ('BRG-9425', 'Indomie', 50);

-- ----------------------------
-- Table structure for tblbarangkeluar
-- ----------------------------
DROP TABLE IF EXISTS `tblbarangkeluar`;
CREATE TABLE `tblbarangkeluar`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `iduser` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `namauser` varchar(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kodebarang` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `namabarang` varchar(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tujuan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `kodebarang`(`kodebarang`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tblbarangkeluar
-- ----------------------------
INSERT INTO `tblbarangkeluar` VALUES (6, 'BK-26817', 'KRY-3621', 'Robi', '18-06-2021', 'BRG-3452', 'Taro', 'Ruko Zilva', 10);
INSERT INTO `tblbarangkeluar` VALUES (7, 'BK-42169', 'KRY-3621', 'Robi', '18-06-2021', 'BRG-3452', 'Taro', 'Ruko Sarijadi', 5);
INSERT INTO `tblbarangkeluar` VALUES (8, 'BK-81356', 'KRY-3621', 'Robi', '19-06-2021', 'BRG-9425', 'Indomie', 'Ruko Zilva', 2);
INSERT INTO `tblbarangkeluar` VALUES (9, 'BK-26091', 'KRY-3621', 'Robi', '19-06-2021', 'BRG-9425', 'Indomie', 'Ruko Tamansari A6', 4);
INSERT INTO `tblbarangkeluar` VALUES (10, 'BK-21785', 'KRY-3621', 'Robi', '19-06-2021', 'BRG-9425', 'Indomie', 'Ruko Tamansari', 25);
INSERT INTO `tblbarangkeluar` VALUES (11, 'BK-97513', 'KRY-3621', 'Robi', '19-06-2021', 'BRG-6372', 'Permen', 'Ruko Tamansari', 2);
INSERT INTO `tblbarangkeluar` VALUES (12, 'BK-57629', 'KRY-3621', 'Robi', '19-06-2021', 'BRG-9043', 'Potatos', 'Ruko Taman Anggrek', 2);
INSERT INTO `tblbarangkeluar` VALUES (13, 'BK-45381', 'KRY-3621', 'Robi', '21-06-2021', 'BRG-0871', 'Sunsilk', 'Ruko Taman Anggrek', 5);
INSERT INTO `tblbarangkeluar` VALUES (14, 'BK-29107', 'KRY-4821', 'Iip', '25-06-2021', 'BRG-7025', 'Teh Pucuk', 'Indoapril', 5);
INSERT INTO `tblbarangkeluar` VALUES (15, 'BK-93710', 'KRY-2586', 'kartap', '24-08-2021', 'BRG-0291', 'rokok', 'Indoapril', 3);
INSERT INTO `tblbarangkeluar` VALUES (18, 'BK-58074', 'KRY-3621', 'Robi', '28-08-2021', 'BRG-9425', 'Indomie', 'Gedung Indo', 2);

-- ----------------------------
-- Table structure for tblbarangmasuk
-- ----------------------------
DROP TABLE IF EXISTS `tblbarangmasuk`;
CREATE TABLE `tblbarangmasuk`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `iduser` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `namauser` varchar(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kodebarang` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `namabarang` varchar(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tujuan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `iduser`(`iduser`) USING BTREE,
  INDEX `kodebarang`(`kodebarang`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tblbarangmasuk
-- ----------------------------
INSERT INTO `tblbarangmasuk` VALUES (4, 'BM-87305', 'KRY-3621', 'Robi', 'BRG-9425', 'Indomie', '11-06-2021', 'Gudang Dago', 50);
INSERT INTO `tblbarangmasuk` VALUES (5, 'BM-30986', 'KRY-3621', 'Robi', 'BRG-5619', 'Sarimi', '11-06-2021', 'Kantor Sutami', 25);
INSERT INTO `tblbarangmasuk` VALUES (10, 'BM-71326', 'KRY-3621', 'Robi', 'BRG-3452', 'Taro', '12-06-2021', 'Warehouse ABC', 20);
INSERT INTO `tblbarangmasuk` VALUES (33, 'BM-38492', 'KRY-3621', 'Robi', 'BRG-0871', 'Sunsilk', '22-06-2021', 'Kantor Sutami', 6);
INSERT INTO `tblbarangmasuk` VALUES (35, 'BM-31529', 'KRY-3621', 'Robi', 'BRG-5619', 'Sarimi', '23-06-2021', 'Warehouse ABC', 20);
INSERT INTO `tblbarangmasuk` VALUES (39, 'BM-18765', 'KRY-4821', 'Iip', 'BRG-6372', 'Permen', '25-06-2021', 'Gudang Dago', 15);
INSERT INTO `tblbarangmasuk` VALUES (40, 'BM-52790', 'KRY-4821', 'Iip', 'BRG-7025', 'Teh Pucuk', '25-06-2021', 'Warehouse ABC', 25);
INSERT INTO `tblbarangmasuk` VALUES (42, 'BM-50968', 'KRY-2586', 'kartap', 'BRG-0291', 'rokok', '24-08-2021', 'Gudang Tamrin', 5);
INSERT INTO `tblbarangmasuk` VALUES (43, 'BM-72845', 'KRY-3621', 'Robi', 'BRG-9043', 'Potatos', '28-08-2021', 'Warehouse ABC', 100);

-- ----------------------------
-- Table structure for tbluser
-- ----------------------------
DROP TABLE IF EXISTS `tbluser`;
CREATE TABLE `tbluser`  (
  `iduser` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `namauser` varchar(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat` varchar(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `telepon` varchar(16) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `akses` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`iduser`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbluser
-- ----------------------------
INSERT INTO `tbluser` VALUES ('KRY-2586', 'kartap', 'kartap', '+62821234455', 'kartap1', 'kartap1', 'karyawan', 'aktif');
INSERT INTO `tbluser` VALUES ('KRY-3621', 'Robi', 'Bantarujeg', '+62822345678', 'robi', 'robi1', 'admin', 'aktif');
INSERT INTO `tbluser` VALUES ('KRY-4821', 'Iip', 'Majalengka', '+62345678', 'ikuna', 'ikuna', 'admin', 'aktif');

-- ----------------------------
-- Triggers structure for table tblbarangkeluar
-- ----------------------------
DROP TRIGGER IF EXISTS `kurangstok`;
delimiter ;;
CREATE TRIGGER `kurangstok` AFTER INSERT ON `tblbarangkeluar` FOR EACH ROW BEGIN
UPDATE tblbarang SET stok = stok - NEW.jumlah WHERE kodebarang = NEW.kodebarang; END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table tblbarangmasuk
-- ----------------------------
DROP TRIGGER IF EXISTS `addstok`;
delimiter ;;
CREATE TRIGGER `addstok` AFTER INSERT ON `tblbarangmasuk` FOR EACH ROW BEGIN
UPDATE tblbarang SET stok = stok + NEW.jumlah WHERE kodebarang = NEW.kodebarang;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table tblbarangmasuk
-- ----------------------------
DROP TRIGGER IF EXISTS `ubah`;
delimiter ;;
CREATE TRIGGER `ubah` AFTER UPDATE ON `tblbarangmasuk` FOR EACH ROW BEGIN
UPDATE tblbarang SET stok = NEW.jumlah WHERE kodebarang = NEW.kodebarang;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
