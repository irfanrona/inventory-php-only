-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 25, 2021 at 02:34 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblbarang`
--

CREATE TABLE `tblbarang` (
  `kodebarang` varchar(8) NOT NULL,
  `namabarang` varchar(35) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbarang`
--

INSERT INTO `tblbarang` (`kodebarang`, `namabarang`, `stok`) VALUES
('BRG-0251', 'Teh Gelas', 0),
('BRG-0871', 'Sunsilk', 6),
('BRG-3452', 'Taro', 19),
('BRG-5619', 'Sarimi', 20),
('BRG-6372', 'Permen', 16),
('BRG-7025', 'Teh Pucuk', 20),
('BRG-9043', 'Potatos', 2),
('BRG-9425', 'Indomie', 50);

-- --------------------------------------------------------

--
-- Table structure for table `tblbarangkeluar`
--

CREATE TABLE `tblbarangkeluar` (
  `id` int(11) NOT NULL,
  `kode` varchar(8) NOT NULL,
  `iduser` varchar(8) NOT NULL,
  `namauser` varchar(35) NOT NULL,
  `tanggal` varchar(20) NOT NULL,
  `kodebarang` varchar(8) NOT NULL,
  `namabarang` varchar(35) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbarangkeluar`
--

INSERT INTO `tblbarangkeluar` (`id`, `kode`, `iduser`, `namauser`, `tanggal`, `kodebarang`, `namabarang`, `jumlah`) VALUES
(6, 'BK-26817', 'KRY-3621', 'Robi', '18-06-2021', 'BRG-3452', 'Taro', 10),
(7, 'BK-42169', 'KRY-3621', 'Robi', '18-06-2021', 'BRG-3452', 'Taro', 5),
(8, 'BK-81356', 'KRY-3621', 'Robi', '19-06-2021', 'BRG-9425', 'Indomie', 2),
(9, 'BK-26091', 'KRY-3621', 'Robi', '19-06-2021', 'BRG-9425', 'Indomie', 3),
(10, 'BK-21785', 'KRY-3621', 'Robi', '19-06-2021', 'BRG-9425', 'Indomie', 25),
(11, 'BK-97513', 'KRY-3621', 'Robi', '19-06-2021', 'BRG-6372', 'Permen', 2),
(12, 'BK-57629', 'KRY-3621', 'Robi', '19-06-2021', 'BRG-9043', 'Potatos', 2),
(13, 'BK-45381', 'KRY-3621', 'Robi', '21-06-2021', 'BRG-0871', 'Sunsilk', 5),
(14, 'BK-29107', 'KRY-4821', 'Iip', '25-06-2021', 'BRG-7025', 'Teh Pucuk', 5);

--
-- Triggers `tblbarangkeluar`
--
DELIMITER $$
CREATE TRIGGER `kurangstok` AFTER INSERT ON `tblbarangkeluar` FOR EACH ROW BEGIN
UPDATE tblbarang SET stok = stok - NEW.jumlah WHERE kodebarang = NEW.kodebarang; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tblbarangmasuk`
--

CREATE TABLE `tblbarangmasuk` (
  `id` int(11) NOT NULL,
  `kode` varchar(8) NOT NULL,
  `iduser` varchar(8) NOT NULL,
  `namauser` varchar(35) NOT NULL,
  `kodebarang` varchar(8) NOT NULL,
  `namabarang` varchar(35) NOT NULL,
  `tanggal` varchar(20) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbarangmasuk`
--

INSERT INTO `tblbarangmasuk` (`id`, `kode`, `iduser`, `namauser`, `kodebarang`, `namabarang`, `tanggal`, `jumlah`) VALUES
(4, 'BM-87305', 'KRY-3621', 'Robi', 'BRG-9425', 'Indomie', '11-06-2021', 50),
(5, 'BM-30986', 'KRY-3621', 'Robi', 'BRG-5619', 'Sarimi', '11-06-2021', 25),
(10, 'BM-71326', 'KRY-3621', 'Robi', 'BRG-3452', 'Taro', '12-06-2021', 20),
(33, 'BM-38492', 'KRY-3621', 'Robi', 'BRG-0871', 'Sunsilk', '22-06-2021', 6),
(35, 'BM-31529', 'KRY-3621', 'Robi', 'BRG-5619', 'Sarimi', '23-06-2021', 20),
(39, 'BM-18765', 'KRY-4821', 'Iip', 'BRG-6372', 'Permen', '25-06-2021', 15),
(40, 'BM-52790', 'KRY-4821', 'Iip', 'BRG-7025', 'Teh Pucuk', '25-06-2021', 25);

--
-- Triggers `tblbarangmasuk`
--
DELIMITER $$
CREATE TRIGGER `addstok` AFTER INSERT ON `tblbarangmasuk` FOR EACH ROW BEGIN
UPDATE tblbarang SET stok = stok + NEW.jumlah WHERE kodebarang = NEW.kodebarang;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ubah` AFTER UPDATE ON `tblbarangmasuk` FOR EACH ROW BEGIN
UPDATE tblbarang SET stok = NEW.jumlah WHERE kodebarang = NEW.kodebarang;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `iduser` varchar(8) NOT NULL,
  `namauser` varchar(35) NOT NULL,
  `alamat` varchar(35) NOT NULL,
  `telepon` varchar(16) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL,
  `akses` varchar(8) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`iduser`, `namauser`, `alamat`, `telepon`, `username`, `password`, `akses`, `status`) VALUES
('KRY-3621', 'Robi', 'Bantarujeg', '+62822345678', 'robi', 'robi1', 'admin', 'aktif'),
('KRY-4821', 'Iip', 'Majalengka', '+62345678', 'ikuna', 'ikuna', 'admin', 'aktif');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblbarang`
--
ALTER TABLE `tblbarang`
  ADD PRIMARY KEY (`kodebarang`);

--
-- Indexes for table `tblbarangkeluar`
--
ALTER TABLE `tblbarangkeluar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kodebarang` (`kodebarang`);

--
-- Indexes for table `tblbarangmasuk`
--
ALTER TABLE `tblbarangmasuk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduser` (`iduser`),
  ADD KEY `kodebarang` (`kodebarang`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`iduser`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblbarangkeluar`
--
ALTER TABLE `tblbarangkeluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tblbarangmasuk`
--
ALTER TABLE `tblbarangmasuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
