<?php
include "koneksi.php";
session_start();
if (!isset($_SESSION['username'])) {
    header("location:login.php");
} else {
    if ($data1['akses'] != 'admin') {
        header("location:barang/barang.php");
    }
}
?>
<?php
include "koneksi.php";
$quey1 = @mysqli_query($konek, "SELECT * FROM `tbluser` WHERE `username`='$_SESSION[username]'");
$data1 = mysqli_fetch_array($quey1);


include "dashboard_data.php";
?>
<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Beranda</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width-device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="dist/css/bootstrap.min.css">
    <link rel="shortcut icon" href="img/favicon.png">
</head>

<body style="background-color:#f5f5f5;">

    <div class="header">
        <div class="container-fluid">
            <div class="row">
                <div class="logo col-md-2">INVENTORY</div>
                <div class="user col-md-10">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <?php echo $_SESSION['username']; ?>&nbsp;&nbsp;
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="edit.php"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>&nbsp;&nbsp;Ubah
                                    Password</a></li>
                            <li><a href="logout.php"><span class="glyphicon glyphicon-off" aria-hidden="true"></span>&nbsp;&nbsp;Keluar</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hideung">
        <div class="container-fluid">
            <div class="row">
                <div class="wkt col-md-2">Hari ini :&nbsp;<?php date_default_timezone_set("Asia/Jakarta");
                                                            echo "" . date("d F Y"); ?></div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="judul">
                    <h4>INVETORY SYSTEM</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-md-2 menu col-sm-3">
                <ul>
                    <?php
                    if ($data1['akses'] == 'admin') {
                    ?>
                        <li class="active"><a href="index.php"><span class="glyphicon glyphicon-dashboard" aria-hidden="true">
                                </span>&nbsp;&nbsp;Dashboard</a></li>

                        <li><a href="user/user.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;&nbsp;User
                                Management</a></li>
                    <?php
                    }
                    ?>
                    <li><a href="barang/barang.php"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>&nbsp;&nbsp;Data
                            Barang</a></li>
                    <li><a href="barang_m/barang_m.php"><span class="glyphicon glyphicon-import" aria-hidden="true"></span>&nbsp;&nbsp;Data Barang
                            Masuk</a></li>
                    <li><a href="barang_k/barang_k.php"><span class="glyphicon glyphicon-export" aria-hidden="true"></span>&nbsp;&nbsp;Data Barang
                            Keluar</a></li>
                    <?php
                    if ($data1['akses'] == 'admin') {
                    ?>
                        <li style="border-bottom:solid 1px #bbbbbb;"><a href="laporan/laporan.php"><span class="glyphicon glyphicon-file" aria-hidden="true">
                                </span>&nbsp;&nbsp;Laporan</a></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Selamat Datang di Sistem Informasi Inventory</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="box-info">
                            <div class="card bg-light mb-3" style="max-width: 18rem;">
                                <div class="card-header"></div>
                                <div class="card-body">
                                    <h5 class="card-title">Jumlah Variasi Semua Barang</h5>
                                    <h1>
                                        <span class="glyphicon glyphicon glyphicon-shopping-cart">
                                        </span>

                                        <?php echo $data['totalVariasiBarang']; ?>
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="box-info">
                            <div class="card bg-light mb-3" style="max-width: 18rem;">
                                <div class="card-header"></div>
                                <div class="card-body">
                                    <h5 class="card-title">Jumlah Transaksi Barang Masuk</h5>
                                    <h1>
                                        <span class="glyphicon glyphicon-download">
                                        </span>

                                        <?php echo $data['totalBarangMasuk']; ?>
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="box-info">
                            <div class="card bg-light mb-3" style="max-width: 18rem;">
                                <div class="card-header"></div>
                                <div class="card-body">
                                    <h5 class="card-title">Jumlah Transaksi Barang Keluar</h5>
                                    <h1>
                                        <span class="glyphicon glyphicon-upload">
                                        </span>

                                        <?php echo $data['totalBarangKeluar']; ?>
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="box-info">
                            <div class="card bg-light mb-6" style="max-width: 40rem;">
                                <div class="card-header"></div>
                                <div class="card-body">
                                    <h4 class="card-title"><span class="glyphicon glyphicon-gift">
                                        </span> Barang Baru</h4>
                                    <table class="table table-striped">
                                        <thead>
                                            <td>Nama</td>
                                            <td>Kode</td>
                                            <td>Stok</td>
                                        </thead>
                                        <tr>
                                            <td><?php echo $data['barangBaru']['namabarang']; ?></td>
                                            <td><?php echo $data['barangBaru']['kodebarang']; ?></td>
                                            <td><?php echo $data['barangBaru']['stok']; ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="box-info">
                            <div class="card bg-light mb-3" style="max-width: 40rem;">
                                <div class="card-header"></div>
                                <div class="card-body">
                                    <h4 class="card-title"><span class="glyphicon glyphicon-user">
                                        </span> Karyawan Baru</h4>
                                    <table class="table table-striped">
                                        <thead>
                                            <td>Nama</td>
                                            <td>Akses</td>
                                            <td>Status</td>
                                        </thead>
                                        <tr>
                                            <td><?php echo $data['userBaru']['namauser']; ?></td>
                                            <td><?php echo $data['userBaru']['akses']; ?></td>
                                            <td><?php echo $data['userBaru']['status']; ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

</body>

<footer>
    &copy;&nbsp;Roby Fuadi Zulva
</footer>

</html>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>