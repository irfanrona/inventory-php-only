<?php
include "../koneksi.php";
session_start();
if (!isset($_SESSION['username'])) {
    header("location:login.php");
}
?>
<?php
include "../koneksi.php";
$quey1 = @mysqli_query($konek, "SELECT * FROM `tbluser` WHERE `username`='$_SESSION[username]'");
$data1 = mysqli_fetch_array($quey1);
?>
<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Data Barang Masuk</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width-device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.min.css">
    <link rel="shortcut icon" href="../img/favicon.png">
</head>

<body style="background-color:#f5f5f5;">

    <div class="header">
        <div class="container-fluid">
            <div class="row">
                <div class="logo col-md-2">INVENTORY</div>
                <div class="user col-md-10">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria- haspopup="true" aria-expanded="true">
                            <?php echo $_SESSION['username']; ?>&nbsp;&nbsp;
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="../edit.php"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>&nbsp;&nbsp;Ubah
                                    Password</a></li>
                            <li><a href="../logout.php"><span class="glyphicon glyphicon-off" aria-hidden="true">
                                    </span>&nbsp;&nbsp;Keluar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hideung">
        <div class="container-fluid">
            <div class="row">
                <div class="wkt col-md-2">Hari ini :&nbsp;<?php date_default_timezone_set("Asia/Jakarta");
                                                            echo "" . date("d F Y"); ?></div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="judul">
                    <h4>DATA BARANG MASUK</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-md-2 menu col-sm-3">
                <?php

                ?>

                <ul>
                    <?php
                    if ($data1['akses'] == 'admin') {
                    ?>
                        <li><a href="../index.php"><span class="glyphicon glyphicon-dashboard" aria-hidden="true">
                                </span>&nbsp;&nbsp;Dashboard</a></li>

                        <li><a href="../user/user.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;&nbsp;User
                                Management</a></li>
                    <?php
                    }
                    ?>
                    <li><a href="../barang/barang.php"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>&nbsp;&nbsp;Data
                            Barang</a></li>
                    <li class="active"><a href="#"><span class="glyphicon glyphicon-import" aria-hidden="true"></span>&nbsp;&nbsp;Data Barang Masuk</a></li>
                    <li><a href="../barang_k/barang_k.php"><span class="glyphicon glyphicon-export" aria-hidden="true"></span>&nbsp;&nbsp;Data Barang
                            Keluar</a></li>
                    <?php
                    if ($data1['akses'] == 'admin') {
                    ?>
                        <li style="border-bottom:solid 1px #bbbbbb;"><a href="../laporan/laporan.php"><span class="glyphicon glyphicon-file" aria-hidden="true">
                                </span>&nbsp;&nbsp;Laporan</a></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="col-md-7 col-md-offset-2 col-sm-6 col-sm-offset-3">

                <?php
                if ($data1['akses'] == 'admin') {
                ?>

                    <div class="kiri">
                        <table class="table-responsive" style="width:100%;">
                            <tr>
                                <td>
                                    <a href="print.php" target=_blank><button class="btn btn-success" style="height:30px;">Cetak</button></a>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <form action=barang_m.php method=GET><input type="text" class="cari" name="search" style="width:20%; height:30px; border-radius:2px; border: solid 1px #cccccc;">
                                        <button class="btn btn-primary" style="height:30px;">Cari</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <?php
                        // jika ada get cari maka nganu
                        if (isset($_GET['cari'])) : ?>
                            <div class="alert alert-info"><a>Pencarian untuk <?php echo $_GET['cari']; ?></a>
                                <a href="barang_m.php?paging=1" class="close">x</a>
                            </div>
                        <?php endif ?>

                        <?php
                        include "../koneksi.php";
                        echo '
            <table class="table table-bordered table-responsive table-hover">
            <thead>
                <tr>
                    <th><center>NO</center></th>
                    <th><center>ID MASUK</center></th>
                    <th><center>ID USER</center></th>
                    <th><center>NAMA USER</center></th>
                    <th><center>TANGGAL</center></th>
                    <th><center>KODE BARANG</center></th>
                    <th><center>NAMA BARANG</center></th>
                    <th><center>TUJUAN</center></th>
                    <th><center>JUMLAH</center></th>
                    <th><center>AKSI</center></th>
                </tr>
            </thead>';

                        $offset = 10;
                        $this_page = !empty($_GET['paging']) ? $_GET['paging'] : '1';
                        $start = (($this_page - 1) * $offset);
                        // membuat sql
                        $sql = "SELECT * FROM `tblbarangmasuk`";
                        // jika ada var get cari
                        if (isset($_GET['search'])) {
                            $sql .= "WHERE `kode` LIKE '%$_GET[search]%' OR `namauser` LIKE '%$_GET[search]%' OR `namabarang` LIKE '%$_GET[search]%' OR `tanggal` LIKE '%$_GET[search]%' OR `tujuan` LIKE '%$_GET[search]%'";
                        }
                        // melanjutkan sql
                        $sql .= "ORDER BY `id` ASC LIMIT $start, $offset";
                        $i = 0;
                        $query = mysqli_query($konek, $sql);
                        while ($data = mysqli_fetch_array($query)) {
                            $i++;
                            echo "<tr>
                <td>" . $i . "</td>
                <td>" . $data['kode'] . "</td>
                <td>" . $data['iduser'] . "</td>
                <td>" . $data['namauser'] . "</td>
                <td>" . $data['tanggal'] . "</td>
                <td>" . $data['kodebarang'] . "</td>
                <td>" . $data['namabarang'] . "</td>
                <td>" . $data['tujuan'] . "</td>
                <td>" . $data['jumlah'] . "</td>
                <td><a title='Edit' href='edit.php?id=" . $data['id'] . "'><span class='glyphicon glyphicon-pencil'></span></a> | <a title='Hapus' href='hapus.php?id=" . $data['id'] . "' onclick=\"return confirm('Apakah Anda Yakin Akan Menghapus Data ini?')\"><span class='glyphicon glyphicon-trash'></span></a></td>
                </tr>";
                        }
                        echo '</table>';
                        ?>

                        <ul class="pager">
                            <?php
                            // membuat sql untuk menghitung semua data
                            $sql2 = "SELECT * FROM `tblbarangmasuk`";
                            if (isset($_GET['search'])) {
                                // jika ada var get cari
                                $sql2 .= "WHERE `kode` LIKE '%$_GET[search]%' OR `namauser` LIKE '%$_GET[search]%' OR `namabarang` LIKE '%$_GET[search]%' OR `tanggal` LIKE '%$_GET[search]%' OR `tujuan` LIKE '%$_GET[search]%'";
                            }
                            // lanjut sql
                            $cari = isset($_GET['search']) ? '&cari=' . $_GET['search'] : '';

                            // jml data semua
                            $jml_data = mysqli_num_rows(mysqli_query($konek, $sql2));
                            // jml halaman
                            $jml_hal = ceil($jml_data / $offset);

                            if ($this_page > 1) {
                                echo '<li class="previous"><a class="withripple" style="border-radius:1px;" href="barang_m.php?paging=' . ($this_page - 1) . '' . $cari . '">← Sebelumnya</a></li>';
                            }

                            if ($this_page < $jml_hal) {
                                echo ' <li class="next"><a class="withripple" style="border-radius:1px;" href="barang_m.php?paging=' . ($this_page + 1) . '' . $cari . '">Selanjutnya →</a></li>';
                            }
                            ?>
                        </ul>

                    </div>

                <?php
                } else {
                ?>

                    <div class="kiri">
                        <table class="table-responsive" style="width:100%;">
                            <tr>
                                <td>
                                    <a href="print.php" target=_blank><button class="btn btn-success" style="height:30px;">Cetak</button></a>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <form action=barang_m.php method=GET><input type="text" class="cari" name="search" style="width:20%; height:30px; border-radius:2px; border: solid 1px #cccccc;">
                                        <button class="btn btn-primary" style="height:30px;">Cari</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <?php
                        // jika ada get cari maka nganu
                        if (isset($_GET['cari'])) : ?>
                            <div class="alert alert-info"><a>Pencarian untuk <?php echo $_GET['cari']; ?></a>
                                <a href="barang_m.php?paging=1" class="close">x</a>
                            </div>
                        <?php endif ?>

                        <?php
                        include "../koneksi.php";
                        echo '
            <table class="table table-bordered table-responsive table-hover">
            <thead>
                <tr>
                    <th><center>NO</center></th>
                    <th><center>ID MASUK</center></th>
                    <th><center>ID USER</center></th>
                    <th><center>NAMA USER</center></th>
                    <th><center>TANGGAL</center></th>
                    <th><center>KODE BARANG</center></th>
                    <th><center>NAMA BARANG</center></th>
                    <th><center>TUJUAN</center></th>
                    <th><center>JUMLAH</center></th>
                </tr>
            </thead>';

                        $offset = 10;
                        $this_page = !empty($_GET['paging']) ? $_GET['paging'] : '1';
                        $start = (($this_page - 1) * $offset);
                        // membuat sql
                        $sql = "SELECT * FROM `tblbarangmasuk`";
                        // jika ada var get cari
                        if (isset($_GET['search'])) {
                            $sql .= "WHERE `kode` LIKE '%$_GET[search]%' OR `namauser` LIKE '%$_GET[search]%' OR `namabarang` LIKE '%$_GET[search]%' OR `tanggal` LIKE '%$_GET[search]%' OR `tujuan` LIKE '%$_GET[search]%'";
                        }
                        // melanjutkan sql
                        $sql .= "ORDER BY `id` ASC LIMIT $start, $offset";
                        $i = 0;
                        $query = mysqli_query($konek, $sql);
                        while ($data = mysqli_fetch_array($query)) {
                            $i++;
                            echo "<tr>
                <td>" . $i . "</td>
                <td>" . $data['kode'] . "</td>
                <td>" . $data['iduser'] . "</td>
                <td>" . $data['namauser'] . "</td>
                <td>" . $data['tanggal'] . "</td>
                <td>" . $data['kodebarang'] . "</td>
                <td>" . $data['namabarang'] . "</td>
                <td>" . $data['tujuan'] . "</td>
                <td>" . $data['jumlah'] . "</td>
                </tr>";
                        }
                        echo '</table>';
                        ?>

                        <ul class="pager">
                            <?php
                            // membuat sql untuk menghitung semua data
                            $sql2 = "SELECT * FROM `tblbarangmasuk`";
                            if (isset($_GET['search'])) {
                                // jika ada var get cari
                                $sql2 .= "WHERE `kode` LIKE '%$_GET[search]%' OR `namauser` LIKE '%$_GET[search]%' OR `namabarang` LIKE '%$_GET[search]%' OR `tanggal` LIKE '%$_GET[search]%' OR `tujuan` LIKE '%$_GET[search]%'";
                            }
                            // lanjut sql
                            $cari = isset($_GET['search']) ? '&cari=' . $_GET['search'] : '';

                            // jml data semua
                            $jml_data = mysqli_num_rows(mysqli_query($konek, $sql2));
                            // jml halaman
                            $jml_hal = ceil($jml_data / $offset);

                            if ($this_page > 1) {
                                echo '<li class="previous"><a class="withripple" style="border-radius:1px;" href="barang_m.php?paging=' . ($this_page - 1) . '' . $cari . '">← Sebelumnya</a></li>';
                            }

                            if ($this_page < $jml_hal) {
                                echo ' <li class="next"><a class="withripple" style="border-radius:1px;" href="barang_m.php?paging=' . ($this_page + 1) . '' . $cari . '">Selanjutnya →</a></li>';
                            }
                            ?>
                        </ul>

                    </div>
                <?php
                }
                ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <div class="kanan">
                    <h4>TAMBAH BARANG MASUK</h4><br>
                    <form method="post" action="simpan.php">
                        Id<br>
                        <?php
                        $char   = "0123456789";
                        $postid = substr(str_shuffle($char), 0, 5);
                        ?>
                        <input type="text" class="form-control" value="BM-<?php echo $postid; ?>" name="kode" readonly><br>
                        <?php
                        include "../koneksi.php";
                        $quey1 = @mysqli_query($konek, "SELECT `iduser`, `namauser` FROM `tbluser` WHERE `username`='$_SESSION[username]'");
                        $data1 = mysqli_fetch_array($quey1);

                        ?>
                        <input type="hidden" class="form-control" value="<?php echo $data1['iduser']; ?>" name="iduser" readonly>
                        <input type="hidden" class="form-control" value="<?php echo $data1['namauser']; ?>" name="namauser" readonly>

                        <?php
                        include "../koneksi.php";
                        $sql = mysqli_query($konek, "select * from tblbarang");
                        $arrayjs = "var varkodebarang = new Array();\n";

                        echo 'Nama Barang
                        <br>
                            <select class="form-control col-md-7 col-xs-12" name="namabarang" id="namabarang"        onchange="changeValue(this.value)">;
                            <option value="#">Pilih Barang</option>';
                        while ($data1 = mysqli_fetch_array($sql)) {
                            echo "<option value=\"" . $data1['namabarang'] . "\">" . $data1['namabarang'] . "</option>";
                            $arrayjs .= "varkodebarang['" . $data1['namabarang'] . "']={vkodebarang:'" . addslashes($data1['kodebarang']) . "'};\n";
                        }
                        ?>
                        </select>
                        <input type="hidden" name="kodebarang" id="kodebarang" value="<?php echo "$data1[vkodebarang]"; ?>" class="form-control" readonly>
                        <script type="text/javascript">
                            <?php
                            echo $arrayjs;
                            ?>

                            function changeValue(vnamabarang) {
                                document.getElementById('kodebarang').value = varkodebarang[vnamabarang].vkodebarang;
                            };
                        </script><br>


                        Tanggal<br>
                        <input type="text" class="form-control" name="tanggal" value="<?php date_default_timezone_set("Asia/Jakarta");
                                                                                        echo "" . date("d-m-Y"); ?>" required><br>
                        Jumlah<br>
                        <input type="number" class="form-control" name="jumlah" autocomplete="0" required><br>
                        Tujuan<br>
                        <input type="text" class="form-control" name="tujuan" autocomplete="0" required><br>
                        <p></p>
                        <input type="submit" class="btn btn-primary" name="button" id="button" value="Simpan">&nbsp;&nbsp;<input type="reset" class="btn btn-danger" name="reset" id="reset" value="Batal">
                    </form>
                </div>
            </div>
</body>

<footer>
    &copy;&nbsp;Roby Fuadi Zulva
</footer>

</html>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>