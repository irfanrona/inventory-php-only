<?php
include "../koneksi.php";
session_start();
if (!isset($_SESSION['username'])){
header ("location:../login.php");
}
?>
<html>
    <head>
        <title>Print</title>
        <script language="Javascript1.2">
            function printpage(){
                window.print();
            }
        </script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width-device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.min.css">
        <link rel="shortcut icon" href="../img/favicon.png">
    </head>
    <body onLoad="printpage()"><center>
        <table class="table table-bordered table-responsive" style="width: 800px;">
            <center><h3>DATA PEGAWAI</h3></center>
            <thead>
                <tr>
                    <th><center>NO</center></th>
                    <th><center>ID</center></th>
                    <th><center>NAMA</center></th>
                    <th><center>ALAMAT</center></th>
                    <th><center>TELEPON</center></th>
                    <th><center>USERNAME</center></th>
                    <th><center>AKSES</center></th>
                    <th><center>STATUS</center></th>
                </tr>
            </thead>
            <?php
                include "../koneksi.php";
                $i = 0;
                $perintah=mysqli_query($konek, "select * from tbluser order by namauser asc");
                while ($data=mysqli_fetch_array($perintah))
                {
                    $i++;
                    echo("<tr>
                        <td>".$i."</td>
                        <td>".$data['iduser']."</td>
                        <td>".$data['namauser']."</td>
                        <td>".$data['alamat']."</td>
                        <td>".$data['telepon']."</td>
                        <td>".$data['username']."</td>
                        <td>".$data['akses']."</td>
                        <td>".$data['status']."</td>
                      </tr>");
                }
            ?>
        </table></center>
    </body>
</html>