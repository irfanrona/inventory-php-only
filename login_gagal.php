<?php
session_start();
if (isset($_SESSION['username'])) {
    header("location:index.php");
}
?>
<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width-device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="dist/css/bootstrap.min.css">
    <link rel="shortcut icon" href="img/favicon.png">
</head>

<body style="background-color: #f3f3f3;">
    <div class="col-md-5 col-md-offset-4">
        <div class="popup">
            <div class="alert alert-danger" role="alert">
                <h4>
                    <?php echo $_GET['pesan']; ?>
                </h4>
            </div>
        </div>

        <div class="sirah">
            <span class="glyphicon glyphicon-lock" style="font-size: 20px;" aria-hidden="true"></span> Login
        </div>
        <div class="login">
            <form action="proses.php" method="post">
                <tr>
                    <td>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                            <input type="text" class="form-control" placeholder="Username" name="username" aria-describedby="basic-addon1" autocomplete="0">
                        </div>
                    </td>
                </tr>
                <br>
                <tr>
                    <td>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-lock"></span></span>
                            <input type="password" class="form-control" placeholder="Password" name="password" aria-describedby="basic-addon1">
                        </div>
                    </td>
                </tr>
                <br>
                <input type="hidden" name="status" value="aktif"></input>
                <br>
                <tr>
                    <td>
                        <input type="submit" class="btn btn-primary" style="width:100%; box-shadow: 3px 7px 10px #222222;" name="button" id="button" value="Masuk">
                    </td>
                </tr>
            </form>
        </div>
    </div>
</body>

</html>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>