<?php
include "../koneksi.php";
session_start();
if (!isset($_SESSION['username'])){
header ("location:../login.php");
}
?>
<html>
    <head>
        <title>Print</title>
        <script language="Javascript1.2">
            function printpage(){
                window.print();
            }
        </script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width-device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.min.css">
        <link rel="shortcut icon" href="../img/favicon.png">
    </head>
    <body onLoad="printpage()"><center>
        <table class="table table-bordered table-responsive" style="width: 800px;">
            <center><h3>DATA BARANG</h3></center>
            <thead>
                <tr>
                    <th><center>NO</center></th>
                    <th><center>KODE</center></th>
                    <th><center>NAMA</center></th>
                    <th><center>JUMLAH</center></th>
                </tr>
            </thead>
            <?php
                include "../koneksi.php";
                $i = 0;
                $perintah=mysqli_query($konek, "select * from tblbarang order by namabarang asc");
                while ($data=mysqli_fetch_array($perintah))
                {
                    $i++;
                    echo("<tr><td align=center>$i</td>
                                <td align=center>$data[kodebarang]</td>
                                <td align=center>$data[namabarang]</td>
                                <td align=center>$data[stok]</td>
                    </tr>");
                }
            ?>
        </table></center>
    </body>
</html>