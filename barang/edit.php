<?php
include "../koneksi.php";
session_start();
if (!isset($_SESSION['username'])){
header ("location:login.php");
}
?>
<?php
include"../koneksi.php";
$quey1 = @mysqli_query($konek, "SELECT * FROM `tbluser` WHERE `username`='$_SESSION[username]'");
$data1= mysqli_fetch_array($quey1);
?>
<!doctype html>
<html>
    
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Data Barang</title>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width-device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.min.css">
        <link rel="shortcut icon" href="../img/favicon.png">
    </head>
    
    <body style="background-color:#f5f5f5;">
        
        <div class="header">
            <div class="container-fluid">
                <div class="row">
                    <div class="logo col-md-2">INVENTORY</div>
                    <div class="user col-md-10">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-
                                    haspopup="true" aria-expanded="true">
                                <?php echo $_SESSION['username']; ?>&nbsp;&nbsp;
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="../edit.php"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>&nbsp;&nbsp;Ubah 
                                    Password</a></li>
                                <li><a href="../logout.php"><span class="glyphicon glyphicon-off" aria-hidden="true">
                                    </span>&nbsp;&nbsp;Keluar</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="hideung">
            <div class="container-fluid">
                <div class="row">
                    <div class="wkt col-md-2">Hari ini :&nbsp;<?php date_default_timezone_set("Asia/Jakarta"); echo"".date("d F Y"); ?></div>
                </div>
            </div>
        </div>
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10 col-md-offset-2">
                    <div class="judul">
                        <h4>DATA BARANG</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
      <div class="row">
        <div class="col-md-3 col-md-2 menu col-sm-3">
            <?php

        ?>

            <ul>
                <li><a href="../index.php"><span class="glyphicon glyphicon-dashboard" aria-hidden="true">
                    </span>&nbsp;&nbsp;Dashboard</a></li>
                <?php
                    if ($data1['akses']=='admin') {
                ?>
                <li><a href="../user/user.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;&nbsp;User
                    Management</a></li>
                <?php
                    }
                ?>
                <li class="active"><a href="#"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>&nbsp;&nbsp;Data
                    Barang</a></li>
                <li><a href="../barang_m/barang_m.php"><span class="glyphicon glyphicon-import" aria-hidden="true"></span>&nbsp;&nbsp;Data Barang Masuk</a></li>
                <li><a href="../barang_k/barang_k.php"><span class="glyphicon glyphicon-export" aria-hidden="true"></span>&nbsp;&nbsp;Data Barang
                    Keluar</a></li>
                <li style="border-bottom:solid 1px #bbbbbb;"><a href="../laporan/laporan.php"><span class="glyphicon glyphicon-file" aria-hidden="true">
                    </span>&nbsp;&nbsp;Laporan</a></li>
            </ul>
        </div>
          
          <?php
                include"../koneksi.php";
                $kode= $_GET['kodebarang'];
                $tampil= mysqli_query($konek,"select * from `tblbarang` where `kodebarang`='$kode'");
                if (mysqli_num_rows($tampil)==0){
                    echo mysqli_errno();
                }else{
                    $data=mysqli_fetch_array($tampil);
                }
            ?>
          
          <div class="col-md-3 col-md-offset-2 col-sm-6 col-sm-offset-3">
          <div class="edit">
            <h4>EDIT DATA BARANG</h4><br>
            <form method="post" action="update.php">
                Kode Barang<br>
                <input type="text" class="form-control" value="<?php echo $data['kodebarang'];?>" name="kodebarang" readonly><br>
                Nama<br>
                <input type="text" class="form-control" value="<?php echo $data['namabarang'];?>" name="namabarang" required><br>
                <p></p>
      <input type="submit" class="btn btn-primary" name="button" id="button" value="Simpan">&nbsp;&nbsp;<input type="reset" class="btn btn-danger" name="reset" id="reset" value="Batal">
            </form>
        </div>
          </div>
    </body>
    
    <footer>
        &copy;&nbsp;Roby Fuadi Zulva
    </footer>
    
</html>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>