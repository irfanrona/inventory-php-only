<?php
include "../koneksi.php";
session_start();
if (!isset($_SESSION['username'])) {
    header("location:../login.php");
}
?>
<html>

<head>
    <title>Print</title>
    <script language="Javascript1.2">
        function printpage() {
            window.print();
        }
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width-device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.min.css">
    <link rel="shortcut icon" href="../img/favicon.png">
</head>

<body onLoad="printpage()">
    <center>
        <table class="table table-bordered table-responsive" style="width: 900px;">
            <center>
                <h3>LAPORAN <?php switch ($_POST['lp1']) {
                                case 'tblbarangmasuk':
                                    echo "DATA BARANG MASUK";
                                    break;

                                case 'tblbarangkeluar':
                                    echo "DATA BARANG KELUAR";
                                    break;
                            } ?></h3>
            </center>
            <thead>
                <tr>
                    <th>
                        <center>NO</center>
                    </th>
                    <th>
                        <center>ID MASUK</center>
                    </th>
                    <th>
                        <center>ID USER</center>
                    </th>
                    <th>
                        <center>NAMA USER</center>
                    </th>
                    <th>
                        <center>TANGGAL</center>
                    </th>
                    <th>
                        <center>KODE BARANG</center>
                    </th>
                    <th>
                        <center>NAMA BARANG</center>
                    </th>
                    <th>
                        <center>TUJUAN</center>
                    </th>
                    <th>
                        <center>JUMLAH</center>
                    </th>
                </tr>
            </thead>
            <?php
            $tabel        = @$_POST['lp1'];
            $tanggal1     = @$_POST['tgl11'];
            $tanggal2     = @$_POST['tgl21'];
            $i = 0;
            $query        = @mysqli_query($konek, "SELECT * FROM `$tabel` WHERE `tanggal` BETWEEN '$tanggal1' AND '$tanggal2'");
            while ($data    = @mysqli_fetch_assoc($query)) {
                $i++;
                echo ("<tr>
                            <td>" . $i . "</td>
                            <td>" . $data['kode'] . "</td>
                            <td>" . $data['iduser'] . "</td>
                            <td>" . $data['namauser'] . "</td>
                            <td>" . $data['tanggal'] . "</td>
                            <td>" . $data['kodebarang'] . "</td>
                            <td>" . $data['namabarang'] . "</td>
                            <td>" . $data['tujuan'] . "</td>
                            <td>" . $data['jumlah'] . "</td>
                      </tr>");
            }
            ?>
        </table>
    </center>
</body>

</html>