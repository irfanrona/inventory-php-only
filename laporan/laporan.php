<?php
include "../koneksi.php";
session_start();
if (!isset($_SESSION['username'])) {
    header("location:login.php");
}
?>
<?php
include "../koneksi.php";
$quey1 = @mysqli_query($konek, "SELECT * FROM `tbluser` WHERE `username`='$_SESSION[username]'");
$data1 = mysqli_fetch_array($quey1);
?>
<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laporan</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css">
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width-device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.min.css">
    <link rel="shortcut icon" href="../img/favicon.png">
</head>

<body style="background-color:#f5f5f5;">

    <div class="header">
        <div class="container-fluid">
            <div class="row">
                <div class="logo col-md-2">INVENTORY</div>
                <div class="user col-md-10">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria- haspopup="true" aria-expanded="true">
                            <?php echo $_SESSION['username']; ?>&nbsp;&nbsp;
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="../edit.php"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>&nbsp;&nbsp;Ubah
                                    Password</a></li>
                            <li><a href="../logout.php"><span class="glyphicon glyphicon-off" aria-hidden="true">
                                    </span>&nbsp;&nbsp;Keluar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hideung">
        <div class="container-fluid">
            <div class="row">
                <div class="wkt col-md-2">Hari ini :&nbsp;<?php date_default_timezone_set("Asia/Jakarta");
                                                            echo "" . date("d F Y"); ?></div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="judul">
                    <h4>LAPORAN</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-md-2 menu col-sm-3">
                <?php

                ?>

                <ul>
                    <li><a href="../index.php"><span class="glyphicon glyphicon-dashboard" aria-hidden="true">
                            </span>&nbsp;&nbsp;Dashboard</a></li>
                    <?php
                    if ($data1['akses'] == 'admin') {
                    ?>
                        <li><a href="../user/user.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;&nbsp;User
                                Management</a></li>
                    <?php
                    }
                    ?>
                    <li><a href="../barang/barang.php"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>&nbsp;&nbsp;Data
                            Barang</a></li>
                    <li><a href="../barang_m/barang_m.php"><span class="glyphicon glyphicon-import" aria-hidden="true"></span>&nbsp;&nbsp;Data Barang
                            Masuk</a></li>
                    <li><a href="../barang_k/barang_k.php"><span class="glyphicon glyphicon-export" aria-hidden="true"></span>&nbsp;&nbsp;Data Barang
                            Keluar</a></li>
                    <li class="active" style="border-bottom:solid 1px #bbbbbb;"><a href="#"><span class="glyphicon glyphicon-file" aria-hidden="true">
                            </span>&nbsp;&nbsp;Laporan</a></li>
                </ul>
            </div>

            <div class="col-md-10 col-md-offset-2">
                <div class="kiri">
                    <script src="../js/jquery-2.2.1.min.js"></script>
                    <script src="../js/jquery-ui.js"></script>

                    <script>
                        $(function() {
                            $("#datepicker3").datepicker({
                                dateFormat: "dd-mm-yy",
                            });
                        });
                        $(function() {
                            $("#datepicker2").datepicker({
                                dateFormat: "dd-mm-yy",
                            });
                        });
                    </script>
                    <form name="form1" method="post" action="">
                        <table class="table-responsive">
                            <tr>
                                <td>Laporan</td>
                                <td>&nbsp;&nbsp;</td>
                                <td>
                                    <select class="form-control" name="lp" id="lp">
                                        <option value="tblbarangmasuk">Barang Masuk</option>
                                        <option value="tblbarangkeluar">Barang Keluar</option>
                                    </select>
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td>Cari tanggal</td>
                                <td>&nbsp;&nbsp;</td>
                                <td>
                                    <input type="text" name="tgl1" id="datepicker3" placeholder="Pilih tanggal.." class="form-control" / autocomplete="off">
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td>Hingga</td>
                                <td>&nbsp;&nbsp;</td>
                                <td>
                                    <input type="text" name="tgl2" id="datepicker2" placeholder="Pilih tanggal.." class="form-control" / autocomplete="off">
                                </td>
                                <td>&nbsp;</td>
                                <td>
                                    <input type="submit" name="submit" value="Cari" class="btn btn-large btn-primary">
                    </form>
                    </td>
                    </tr>
                    </table>
                    </form>
                    <?php
                    $tabel        = @$_POST['lp'];
                    $tanggal1     = @$_POST['tgl1'];
                    $tanggal2     = @$_POST['tgl2'];
                    $i = 0;
                    $query        = @mysqli_query($konek, "SELECT * FROM `$tabel` WHERE `tanggal` BETWEEN '$tanggal1' AND '$tanggal2'");
                    while ($showquery    = @mysqli_fetch_assoc($query)) {
                        $i++;
                    }
                    ?>
                    <br>
                    <table>
                        <td colspan="6">
                            <form action="print.php" target="_blank" method="post">
                                <input type="hidden" name="lp1" value="<?php echo $tabel; ?>">
                                <input type="hidden" name="tgl11" value="<?php echo $tanggal1; ?>">
                                <input type="hidden" name="tgl21" value="<?php echo $tanggal2; ?>">
                                <button type="submit" class="btn btn-success" style="height: 30px;">Cetak</button>
                            </form>
                        </td>
                    </table>
                    <br>
                    <?php
                    echo "Pencarian data $tabel dari tanggal $tanggal1 sampai tanggal $tanggal2";
                    ?>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Id</th>
                                <th>Nama Karyawan</th>
                                <th>Nama Barang</th>
                                <th>Tujuan</th>
                                <th>Jumlah</th>
                                <th>Tanggal</th>
                            </tr>
                        </thead>
                        <?php
                        $tabel        = @$_POST['lp'];
                        $tanggal1     = @$_POST['tgl1'];
                        $tanggal2     = @$_POST['tgl2'];
                        $i = 0;
                        $query        = @mysqli_query($konek, "SELECT * FROM `$tabel` WHERE `tanggal` BETWEEN '$tanggal1' AND '$tanggal2'");
                        while ($showquery    = @mysqli_fetch_assoc($query)) {
                            $i++;
                        ?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $showquery['id']; ?></td>
                                <td><?php echo $showquery['namauser']; ?></td>
                                <td><?php echo $showquery['namabarang']; ?></td>
                                <td><?php echo $showquery['tujuan']; ?></td>
                                <td><?php echo $showquery['jumlah']; ?></td>
                                <td><?php echo $showquery['tanggal']; ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>



                </div>
            </div>
</body>

<footer>
    &copy;&nbsp;Roby Fuadi Zulva
</footer>

</html>
<script src="../js/bootstrap.min.js"></script>