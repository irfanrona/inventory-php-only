<?php
include "koneksi.php";

$sql = "SELECT * FROM `tblbarangkeluar`";
$query = mysqli_query($konek, $sql);

$row = mysqli_num_rows($query);

if ($row) {
	$data['totalBarangKeluar'] = $row;
} else {
	$data['totalBarangKeluar'] = 0;
}
// close the result.
mysqli_free_result($query);

$sql = "SELECT * FROM `tblbarangmasuk`";
$query = mysqli_query($konek, $sql);

$row = mysqli_num_rows($query);

if ($row) {
	$data['totalBarangMasuk'] = $row;
} else {
	$data['totalBarangMasuk'] = 0;
}
// close the result.
mysqli_free_result($query);

$sql = "SELECT * FROM `tblbarang`";
$query = mysqli_query($konek, $sql);

$row = mysqli_num_rows($query);

if ($row) {
	$data['totalVariasiBarang'] = $row;
} else {
	$data['totalVariasiBarang'] = 0;
}
// close the result.
mysqli_free_result($query);


// membuat sql
$sql = "SELECT * FROM `tblbarang`";
$sql .= "ORDER BY `kodebarang` DESC LIMIT 1";

$query = mysqli_query($konek, $sql);
while ($dataRow = mysqli_fetch_array($query)) {

	$data['barangBaru']['kodebarang'] = $dataRow['kodebarang'];
	$data['barangBaru']['namabarang'] = $dataRow['namabarang'];
	$data['barangBaru']['stok'] = $dataRow['stok'];
}

// membuat sql
$sql = "SELECT * FROM `tbluser`";
$sql .= "ORDER BY `iduser` DESC LIMIT 1";

$query = mysqli_query($konek, $sql);
while ($dataRow = mysqli_fetch_array($query)) {

	$data['userBaru']['namauser'] = $dataRow['namauser'];
	$data['userBaru']['iduser'] = $dataRow['iduser'];
	$data['userBaru']['akses'] = $dataRow['akses'];
	$data['userBaru']['status'] = $dataRow['status'];
}
